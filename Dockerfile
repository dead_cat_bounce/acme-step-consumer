FROM bradkarels/base-scala-2.11
MAINTAINER Brad Karels <bkarels@gmail.com>
ENV APPLICATION_SECRET fbe0aa536fc349cbdc451ff5970f9357

#ENV PI /opt/pi
#RUN mkdir $PI
WORKDIR /opt

ADD ./target/universal/pi-template-1.0-SNAPSHOT.zip .

RUN unzip pi-template-1.0-SNAPSHOT.zip && rm pi-template-1.0-SNAPSHOT.zip && mv pi-template-1.0-SNAPSHOT pi

WORKDIR /

CMD ["/opt/pi/bin/pi-template"]
