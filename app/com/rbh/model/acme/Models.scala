package com.rbh.model.acme

case class StepEvent(transactionId: Long,
                     partnerId: Long,
                     nbrOfSteps: Int,
                     receivedTimestamp: Long,
                     eventTimestamp: Long)