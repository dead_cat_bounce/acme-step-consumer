package com.rbh.controllers

import play.api._
import play.api.mvc._
import play.api.libs.json._
import java.time._
import scala.concurrent.{Promise, Future}
import org.apache.kafka.clients.producer.RecordMetadata
//import scala.concurrent.ExecutionContext
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.util.{Success, Failure}
import scala.concurrent.Await
import com.rbh.actors.acme.StepEventActor
import com.rbh.model.acme.StepEvent
import com.rbh.service.AcmePublisher._
import akka.actor._
import javax.inject._
import akka.pattern.ask
import akka.util.Timeout
import scala.concurrent.duration._
import java.util.concurrent.TimeoutException

class FilthPig extends Controller {
  def index = Action {
    Ok("Filthpig")
  }
}

object AcmeReceiver {
  val acmeTimeout = Play.current.configuration.getInt("acme.publish.timeout").getOrElse(1000)
}

// TODO: Authentication
class AcmeReceiver extends Controller {

  def acmeSteps = Action(parse.json) { request =>
    val receivedInstant: Instant = Instant.now

//    implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global

    val body: JsValue = request.body
    // Validate
    val transactionId = (body \ "transactionid").asOpt[Long]
    val partnerId = (body \ "partnerid").asOpt[Long]
    val nbrOfSteps = (body \ "nbrofsteps").asOpt[Int]
    val eventTimestamp = (body \ "eventtimestamp").asOpt[Long]
//    val attributes = (body \ "attributes").asOpt[Map[String, Any]]

    val bodyElements = transactionId :: partnerId :: nbrOfSteps :: eventTimestamp :: Nil

    // TODO: Improve validation.
    val valid = bodyElements.filterNot { _.isDefined }.size == 0
    
    if (valid) {
      // TODO: Log good things...
      val stepEvent = StepEvent(transactionId.get,
        partnerId.get,
        nbrOfSteps.get,
        receivedInstant.toEpochMilli,
        eventTimestamp.get)
        
      val result: Future[RecordMetadata] = publish(stepEvent)
      
      result onComplete {
        case Success(recordMetadata) => {
          println("Success much?")
          // We presume here that these values could be of value for logging, and otherwise...
          val offset: Long = recordMetadata.offset()
          val partition: Int = recordMetadata.partition()
  	  
          // Quick dirty timer thing
          val doneDone: Long = Instant.now.toEpochMilli - stepEvent.receivedTimestamp
          println(s"Done done --> $doneDone millis <---")
        }
        case Failure(t) => {
          println("Failure much?")
          // TODO: Log entire payload to error/retry log
          val msg = t.getMessage
        }
      }
      
      // Return 202 
  	  Accepted
    } else {
      // Log bad things...
      BadRequest("Validation failed.")
    }
  }
}

@Singleton
class AcmeReceiverActor @Inject() (system: ActorSystem) extends Controller {
	val stepEventActor = system.actorOf(StepEventActor.props, "se-actor")
			
			implicit val timeout = Timeout(250.milliseconds)
			
			import com.rbh.model.acme.StepEvent
			
			def acmeSteps = Action(parse.json) { request =>
  			val receivedInstant: Instant = Instant.now
  			
  //    implicit val ec: ExecutionContext = scala.concurrent.ExecutionContext.Implicits.global
  			
  			val body: JsValue = request.body
  			// Validate
  			val transactionId = (body \ "transactionid").asOpt[Long]
  			val partnerId = (body \ "partnerid").asOpt[Long]
  			val nbrOfSteps = (body \ "nbrofsteps").asOpt[Int]
  			val eventTimestamp = (body \ "eventtimestamp").asOpt[Long]
  //    val attributes = (body \ "attributes").asOpt[Map[String, Any]]
  											
  			val bodyElements = transactionId :: partnerId :: nbrOfSteps :: eventTimestamp :: Nil
  											
  			// TODO: Improve validation.
  			val valid = bodyElements.filterNot { _.isDefined }.size == 0
  			
  			if (valid) {
  				// TODO: Log good things...
  				val stepEvent = StepEvent(transactionId.get,
                        						partnerId.get,
                        						nbrOfSteps.get,
                        						receivedInstant.toEpochMilli,
                        						eventTimestamp.get)
  														
  				val response: Future[Any] = stepEventActor ? stepEvent
  				val result: Future[RecordMetadata] = Await.result(response, timeout.duration).asInstanceOf[Future[RecordMetadata]]
  
          result onComplete {
            case Success(recordMetadata) => {
              println("Success much?")
              // We presume here that these values could be of value for logging, and otherwise...
              val offset: Long = recordMetadata.offset()
              val partition: Int = recordMetadata.partition()
            }
            case Failure(t) => {
              println("Failure much?")
              // TODO: Log entire payload to error/retry log
              val msg = t.getMessage
              println(msg)
            }
          }
  				Accepted
  			} else {
  				// Log bad things...
  				BadRequest("Validation failed.")
  			}
											
//    val result = Await.result(future, timeout.duration).asInstanceOf[Int]
//    val json: JsValue = Json.obj("reverse gazintas" -> result)
//    Ok(json)
	}
}