package com.rbh.util

import com.rbh.avro.partner.acme.event.{AcmeStepEvent, Data}
import scala.collection.JavaConversions._
import java.io.ByteArrayOutputStream
import org.apache.avro.io.{Encoder,EncoderFactory}
import org.apache.avro.specific.SpecificDatumWriter

trait AcmeEvents {
  /**
   * Makes an AcmeStepEvent w/o metadata from a StepEvent.
   */
  import com.rbh.model.acme.StepEvent
  val makeAcmeStepEvent: StepEvent => AcmeStepEvent = { stepEvent: StepEvent =>

    val attrmap: java.util.Map[CharSequence, CharSequence] = Map[CharSequence, CharSequence]()

    val data: Data = Data.newBuilder()
      .setAttributes(attrmap)
      .build()

    AcmeStepEvent.newBuilder()
      .setTransactionid(stepEvent.transactionId)
      .setPartnerid(stepEvent.partnerId)
      .setNbrofsteps(stepEvent.nbrOfSteps)
      .setReceivedtimestamp(stepEvent.receivedTimestamp)
      .setEventtimestamp(stepEvent.eventTimestamp)
      .setData(data)
      .build()
  }
  
  val acmeStepEventAsBytes: AcmeStepEvent => Array[Byte] = { acmeStepEvent:AcmeStepEvent =>
    val baos: ByteArrayOutputStream = new ByteArrayOutputStream()
    val encoderFactory: EncoderFactory = new EncoderFactory()
    
    // TODO: Reuse BinaryEncoder (?)
    val encoder: Encoder = encoderFactory.binaryEncoder(baos, null)
    val specificDatumWriter:SpecificDatumWriter[AcmeStepEvent] = new SpecificDatumWriter[AcmeStepEvent](classOf[AcmeStepEvent])
    
    specificDatumWriter.write(acmeStepEvent, encoder)
    encoder.flush
    baos.toByteArray
  }
  
  
}