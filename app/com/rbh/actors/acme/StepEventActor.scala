package com.rbh.actors.acme

import play.api._
import akka.actor._
import com.rbh.avro.partner.acme.event.{AcmeStepEvent, Data}
import com.rbh.model.acme.StepEvent
import scala.collection.JavaConversions._
import java.time._
import java.util.concurrent.TimeoutException
import java.util.Properties
import org.apache.kafka.common.TopicPartition
import org.apache.kafka.clients.producer.internals.Sender
import org.apache.kafka.clients.producer.{Producer, KafkaProducer, ProducerRecord, RecordMetadata}
import com.rbh.util.AcmeEvents
import scala.concurrent.{Promise, Future}
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

object StepEventActor {
  def props = Props[StepEventActor]
  
  
  val systemName = Play.current.configuration.getString("acme.akka.actor.system.name").getOrElse("acmeSystemDefaultName")
  val system = ActorSystem(systemName)
  
  val bootstrapServerList = Play.current.configuration.getString("kafka.bootstrap.servers").getOrElse("this will break something..")
  val acmeTimeout = Play.current.configuration.getInt("acme.publish.timeout").getOrElse(1000)
  
  val producerProps:Properties = new Properties() // producer.type = sync (default)
      producerProps.put("bootstrap.servers", bootstrapServerList)
      producerProps.put("request.required.acks", "-1")
      producerProps.put("request.timeout.ms", "2000")
      producerProps.put("key.serializer", "org.apache.kafka.common.serialization.ByteArraySerializer")
      producerProps.put("value.serializer", "org.apache.kafka.common.serialization.ByteArraySerializer")
      producerProps.put("compression.codec", "snappy")
      producerProps.put("message.send.max.retries", "2")
      producerProps.put("queue.buffering.max.ms", "100")
      producerProps.put("queue.buffering.max.messages", "10000")
      producerProps.put("client.id", "42")
        
  val producer:Producer[Array[Byte], Array[Byte]] = new KafkaProducer[Array[Byte], Array[Byte]](producerProps)
  
//  val errorTopicPartition: TopicPartition = new TopicPartition("ERROR_PARTITION", -1)
//  val errorRecordMetadata: RecordMetadata = new RecordMetadata(errorTopicPartition, -1,-1)
}

class StepEventActor extends Actor with AcmeEvents { 
  import StepEventActor._

  def receive = {
    case se: StepEvent => {
      val acmeStepEvent: AcmeStepEvent = makeAcmeStepEvent(se)
      val bytes:Array[Byte] = acmeStepEventAsBytes(acmeStepEvent)
      val producerRecord = new ProducerRecord[Array[Byte], Array[Byte]]("acmestepevents", bytes)
      val recordMetaData = Future { producer.send(producerRecord).get() } // <-- JFuture
      val timeout = akka.pattern.after(duration = acmeTimeout.milliseconds, using = system.scheduler) {
        Future.failed(new TimeoutException("Acknowledged write to Kafka too more than 500ms."))
      }
      val r: Future[RecordMetadata] = Future.firstCompletedOf(recordMetaData :: timeout :: Nil)
      sender ! r
    }
  } 
}