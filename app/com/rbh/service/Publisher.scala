package com.rbh.service

import play.api._
import java.time._
import com.rbh.avro.partner.acme.event.{AcmeStepEvent, Data}
import scala.collection.JavaConversions._
import java.util.Properties
import java.util.concurrent.{Future => JFuture}
import scala.concurrent.{Promise, Future, ExecutionContext}
import scala.concurrent.duration._
import java.io.ByteArrayOutputStream
import java.util.concurrent.TimeoutException
import org.apache.avro.io.{Encoder,EncoderFactory}
import org.apache.avro.specific.SpecificDatumWriter
import org.apache.kafka.clients.producer.{Producer, KafkaProducer, ProducerRecord, RecordMetadata}
import com.rbh.util.AcmeEvents
import com.rbh.model.acme.StepEvent
import akka.actor.ActorSystem

object AcmePublisher extends AcmeEvents {
  
  val systemName = Play.current.configuration.getString("acme.akka.actor.system.name").getOrElse("acmeSystemDefaultName")
  val system = ActorSystem(systemName)
  
  val bootstrapServerList = Play.current.configuration.getString("kafka.bootstrap.servers").getOrElse("this will break something..")
  val acmeTimeout = Play.current.configuration.getInt("acme.publish.timeout").getOrElse(1000)
  
  val props:Properties = new Properties() // producer.type = sync (default)
      props.put("bootstrap.servers", bootstrapServerList)
      props.put("request.required.acks", "-1")
      props.put("request.timeout.ms", "2000")
      props.put("key.serializer", "org.apache.kafka.common.serialization.ByteArraySerializer")
      props.put("value.serializer", "org.apache.kafka.common.serialization.ByteArraySerializer")
      props.put("compression.codec", "snappy")
      props.put("message.send.max.retries", "2")
      props.put("queue.buffering.max.ms", "100")
      props.put("queue.buffering.max.messages", "10000")
      props.put("client.id", "42")
        
  val producer:Producer[Array[Byte], Array[Byte]] = new KafkaProducer[Array[Byte], Array[Byte]](props)
      
  def publish(evt: StepEvent)(implicit ec: ExecutionContext): Future[RecordMetadata] = {
    val acmeStepEvent: AcmeStepEvent = makeAcmeStepEvent(evt)
    val bytes:Array[Byte] = acmeStepEventAsBytes(acmeStepEvent)
    val producerRecord = new ProducerRecord[Array[Byte], Array[Byte]]("acmestepevents", bytes)
    val recordMetaData = Future { producer.send(producerRecord).get() } // <-- JFuture
    val timeout = akka.pattern.after(duration = acmeTimeout.milliseconds, using = system.scheduler) {
      Future.failed(new TimeoutException("Acknowledged write to Kafka too more than 500ms."))
    }
    Future.firstCompletedOf(recordMetaData :: timeout :: Nil)
  }
}